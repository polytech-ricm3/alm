#include <stdio.h>
#include <math.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/serial_io.h"
#include "sys/clock.h"
#include "usart_put.h"
#include "gpio_led.h"

static volatile int limitms = 1000;
static volatile int h = 0;
static volatile int m = 0;
static volatile int s = 0;
static volatile int startchrono = 0;

void SysTick_init(uint32_t freq)
{
  SysTick.LOAD = ((get_SYSCLK() / freq)-1) & 0x00FFFFFF;
  SysTick.VAL = 0;
  SysTick.CTRL |= 0x07;
}

void __attribute__ ((interrupt)) SysTick_Handler()
{
  static uint32_t compteur = 0;
  compteur++;

  /* 
  //Si le bouton est presse
  if(!(GPIOC.IDR & (1 << 13)))
  {
  
    //Augmentation vitesse LED
    int newlimit = limitms -1;
    if(newlimit <=0)
      newlimit = 500;
    limitms = newlimit;
    
  }
  */
  if(startchrono && compteur >= limitms)
  {
    compteur = 0;
    s++;
    if(s == 60)
    {
      m++;
      s = 0;
    }
    if(m == 60)
    {
      h++;
      m = 0;
    }
    printf("%02d : %02d : %02d\r",h,m,s); //Affichage de chaque nombre sur 2 digits. \r pour revenir en début de ligne
    //GPIOA.ODR ^= 1 << 5; //Clignotement LED
  }
}

void init_USART2_IT()
{
  NVIC.IP[38] = 0xF0;
  NVIC.ISER[38/32] = 1 << (38%32);
}

void __attribute__ ((interrupt)) USART2_Handler()
{
  /* 
  //Reduction vitesse LED
  if((USART2.SR & 0x20) != 0)
  {
    _putchar(USART2.DR & 0x7F) ;
    limitms += 100;
  }
  */
  if((USART2.SR & 0x20) != 0)
  {
    char c = USART2.DR & 0x7F;
    if(c == 'd')
    {
      startchrono = 1;
    }
    else if(c == 'a')
    {
      startchrono = 0;
    }
    else if(c == 'r')
    {
      h = 0;
      s = 0;
      m = 0;
    }
    
  }

}

int main() {
  printf("\e[2J\e[1;1H\r\n*** Welcome to Nucleo F446 ! ***\r\n");
  printf("   %08lx-%08lx-%08lx\r\n",U_ID[0],U_ID[1],U_ID[2]);
  printf("SYSCLK = %9lu Hz\r\n",get_SYSCLK());
  printf("AHBCLK = %9lu Hz\r\n",get_AHBCLK());
  printf("APB1CLK= %9lu Hz\r\n",get_APB1CLK());
  printf("APB2CLK= %9lu Hz\r\n",get_APB2CLK());
  printf("\r\n");
 // exercice1();
//  exercice2();
  init_LED2();
  init_PB();
 
  SysTick_init(1000);
  setup_USART2(9600); //9600 normal 115200 pour chronometre
  USART2.CR1 |= (1<<5); //Activation des interruptions

return 0;
}
