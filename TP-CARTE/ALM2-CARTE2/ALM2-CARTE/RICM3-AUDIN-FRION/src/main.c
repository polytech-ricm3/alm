#include <stdio.h>
#include <math.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/serial_io.h"
#include "sys/init.h"
#include "sys/clock.h"


static volatile int limit = 1000;
static volatile int h = 0;
static volatile int m = 0;
static volatile int s = 0;
static volatile int chrono_running = 0;


//---------------------------------------------------------
// EXO1

void init_LED2()
{
    RCC.AHB1ENR   |= 0x1;
    GPIOA.MODER    = (GPIOA.MODER & ~(0x3 << 10)) | (0x1 << 10);
    GPIOA.OTYPER   = ~(0x1 << 5);
    GPIOA.OSPEEDR |= 0x3 << 10;
}

void init_PB()
{
    RCC.AHB1ENR |= 0x4;
    GPIOC.MODER &= ~(0x3 << 26);
    GPIOC.PUPDR &= ~(0x3 << 26);
}

void tempo_500()
{
    volatile uint32_t time;
    for(time = 0; time < 5600000; time++)
        ;
}
//---------------------------------------------------------


//---------------------------------------------------------
//EXO2

void init_USART(int baudrate)
{
    
    RCC.APB1ENR |= 1 << 17;
    GPIOA.MODER = (GPIOA.MODER & ~(3<<4 | 3 << 6)) | (2 << 4 | 2 << 6);
    GPIOA.AFRL  = (GPIOA.AFRL & ~(0xFF << 8)) | (0x77 << 8); 
    USART2.BRR  = get_APB1CLK() / baudrate; //Rafraichissement cloc
    USART2.CR3  = 0;
    USART2.CR2  = 0;
    USART2.CR1  = (1 << 13) & (1 << 2) & (1 << 3)
}

void _putch(uint8_t c)
{
    while((USART2.SR & 0x80) == 0);
    USART2.DR = c & 0x7F;
}

void _puts(char *string)
{
    while(*string)
        _putch(*string++);
}

char _getc()
{
    while(usart2.SR & Ox20 == 0);
	return USART2.DR & 0x7F
}

void _putint2(uint8_t integer)
{
    uint8_t tmp = integer;
    int nb_char = 0;
    
    while(tmp > 0)
    {
        tmp /= 10;
        nb_char++;
    }
    uint8_t str[nb_char];
    for(int i = nb_char - 1; i > 0; i--)
    {
        str[i] = (char)(integer % 10) + '0';
        integer /= 10; 
    }
    _puts(str);
}

//---------------------------------------------------------


//---------------------------------------------------------
// Exo 3 & 4

void SysTick_init(uint32_t freq)
{
  SysTick.LOAD = ((get_SYSCLK() / freq)-1) & 0x00FFFFFF;
  SysTick.VAL = 0;
  SysTick.CTRL |= 0x07;
}

void init_USART2_IT()
{
  NVIC.IP[38] = 0xF0;
  NVIC.ISER[38/32] = 1 << (38%32);
}

void __attribute__ ((interrupt)) SysTick_Handler()
{
  static uint32_t compteur = 0;
  compteur++;

  /* Exo3
  if(!(GPIOC.IDR & (1 << 13)))
  {
    int newlimit = limit -1;
    if(newlimit <=0)
      newlimit = 500;
    limit = newlimit;
    
  }
  GPIOA.ODR ^= 1 << 5;
  */
  if(chrono_running && compteur >= limit)
  {
    compteur = 0;
    s++;
    if(s == 60)
    {
      m++;
      s = 0;
    }
    if(m == 60)
    {
      h++;
      m = 0;
    }
    printf("%02d : %02d : %02d\r",h,m,s);
  }
}


void __attribute__ ((interrupt)) USART2_Handler()
{
  /* Exo3
  if((USART2.SR & 0x20) != 0)
  {
    _putchar(USART2.DR & 0x7F) ;
    limit += 100;
  }
  */
  if((USART2.SR & 0x20) != 0)
  {
    char c = USART2.DR & 0x7F;
    if(c == 'd')
    {
      chrono_running = 1;
    }
    else if(c == 'a')
    {
      chrono_running = 0;
    }
    else if(c == 'r')
    {
      h = 0;
      s = 0;
      m = 0;
    }
    
  }

}

//---------------------------------------------------------


int main() {

    /*Exo 1
    init_LED2();
    init_PB();
    
    while(1)
    {
        if(GPIOC.IDR & (1 << 13)) // Bouton relacher 
            GPIOA.ODR &= ~(0x1 << 5);
        else
        {
            GPIOA.ODR ^= 1 << 5;
            tempo_500();
        }
    }
    */

    /*Exo2
    init_USART(9600);
    _putint2(12);
    */

    /* Exo3
    init_LED2();
    init_PB();
    setup_USART2(9600);
    void init_USART2_IT();
    */

    SysTick_init(1000);
    setup_USART2(115200);
    USART2.CR1 |= (1<<5); //Activation des interruptions
    return 0;
}

