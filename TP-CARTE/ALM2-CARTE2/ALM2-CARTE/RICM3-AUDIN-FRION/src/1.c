#include <stdio.h>
#include <math.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"


void init_LED2()
{
    RCC.AHB1ENR   |= 0x1;
    GPIOA.MODER    = (GPIOA.MODER & ~(0x3 << 10)) | (0x1 << 10);
    GPIOA.OTYPER   = ~(0x1 << 5);
    GPIOA.OSPEEDR |= 0x3 << 10;
}

void init_PB()
{
    RCC.AHB1ENR |= 0x4;
    GPIOC.MODER &= ~(0x3 << 26);
    GPIOC.PUPDR &= ~(0x3 << 26);
}

void tempo_500()
{
    volatile uint32_t time;
    for(time = 0; time < 5600000; time++)
        ;
}
int main() {
  
  printf("\e[2J\e[1;1H\r\n*** Welcome to Nucleo F446 ! ***\r\n");
  printf("   %08lx-%08lx-%08lx\r\n",U_ID[0],U_ID[1],U_ID[2]);
  printf("SYSCLK = %9lu Hz\r\n",get_SYSCLK());
  printf("AHBCLK = %9lu Hz\r\n",get_AHBCLK());
  printf("APB1CLK= %9lu Hz\r\n",get_APB1CLK());
  printf("APB2CLK= %9lu Hz\r\n",get_APB2CLK());
  printf("\r\n");
  
  init_LED2();
  init_PB();
  
  while(1)
  {
      if(GPIOC.IDR & (1 << 13)) // Bouton relacher 
        GPIOA.ODR &= ~(0x1 << 5);
      else
      {
        GPIOA.ODR ^= 1 << 5;
        tempo_500();
      }
  }
  
  
return 0;
}

