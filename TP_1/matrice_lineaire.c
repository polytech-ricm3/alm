#include "matrice_lineaire.h"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
    matrice m = { l, c, malloc(sizeof(double) * c * l) };
    return m;
}

void liberer_matrice(matrice m) {
    free(m.donnees);
}

int est_matrice_invalide(matrice m) {
    if(m.donnees == NULL || m.l < 0 || m.c < 0)
        return 1;
    int resultat = (sizeof(m.donnees) == (m.c * m.l));
    return resultat;
}

double *acces_matrice(matrice m, int i, int j) {
    double *resultat = &m.donnees[i*m.c + j];
    return resultat;
}

int nb_lignes_matrice(matrice m) {
    int resultat = m.l;
    return resultat;
}

int nb_colonnes_matrice(matrice m) {
    int resultat = m.c;
    return resultat;
}
