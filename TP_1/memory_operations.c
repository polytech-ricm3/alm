#include "memory_operations.h"
#include <stdlib.h>

void *my_memcpy(void *dst, const void *src, size_t len) {
    char *resultat = (char *)dst;
    char *src_c    = (char *)src;
    for(int i = 0; i < len;i++)
    {
        resultat[i] = src_c[i];
    }
    return resultat;
}

void *my_memmove(void *dst, const void *src, size_t len) {
    char *resultat = (char *)dst;
    char *src_c    = (char *) src;
    char *tmp      = malloc(sizeof(char) * len);
    for(int i = 0; i < len; i++)
        tmp[i] = src_c[i];
    for(int i = 0; i < len; i++)
        resultat[i] = tmp[i];
    free(tmp);
    return resultat;
}

int is_little_endian() {
    int x = 1;
    char *y = (char *) &x; 
    return (*y);
}

int reverse_endianess(int value) {
    int x;
	x = ((value & 0x55555555) << 1) | ((value & 0xAAAAAAAA) >> 1);
	x = ((value & 0x33333333) << 2) | ((value & 0xCCCCCCCC) >> 2);
	x = ((value & 0x0F0F0F0F) << 4) | ((value & 0xF0F0F0F0) >> 4);
	x = ((value & 0x00FF00FF) << 8) | ((value & 0xFF00FF00) >> 8);
    return (x << 16) | (x >> 16);

}
