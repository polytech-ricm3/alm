#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v  = malloc(sizeof(struct donnees_vecteur));
    v->taille  = taille;
    v->donnees = malloc(sizeof(double) * taille);
    return v;
}

void liberer_vecteur(vecteur v) {
    free(v->donnees);
    free(v);
}

int est_vecteur_invalide(vecteur v) {
    int resultat = (v == NULL && v->donnees != NULL);
    resultat = (resultat && sizeof(v->donnees) == v->taille);
    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
    double *resultat = NULL;
    if(i >= 0 && i < v->taille)
        resultat = &v->donnees[i];
    else if(i >= v->taille)
    {
        v->donnees = realloc(v->donnees, sizeof(double) * i + 1);
        if(v->donnees != NULL)
        {
            resultat = &v->donnees[i];
            v->taille = i + 1 ;
        }
        else
            resultat = NULL;
    }
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = v->taille;
    return resultat;
}
