#include "vecteur.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = { taille, malloc(sizeof(double) * taille) };
    return v;
}
void liberer_vecteur(vecteur v) {
    free(v.donnees);
}

int est_vecteur_invalide(vecteur v) {
    return v.donnees == NULL;
}

double *acces_vecteur(vecteur v, int i) {
    return (!est_vecteur_invalide(v) && i >= 0 && i < v.taille) ? &v.donnees[i] : NULL;
}

int taille_vecteur(vecteur v) {
    return v.taille;
}
