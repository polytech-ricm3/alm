#include "matrice.h"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
    matrice m = { l, c, malloc(sizeof(double*) * l) };
    if(m.donnees != NULL)
    {
        for(int i = 0; i < l; i++)
        {
            m.donnees[i] = malloc(sizeof(double) * c);
        }
    }
    return m;
}

void liberer_matrice(matrice m) {
    for(int i = 0; i < m.l; i++)
        free(m.donnees[i]);
    free(m.donnees);
}

int est_matrice_invalide(matrice m) {
    if(m.donnees == NULL)
        return 1;
    int i = 0;
    while(i < m.l && m.donnees[i] != NULL){
        i++;
    }
    return i < m.l;
}

double *acces_matrice(matrice m, int i, int j) {
    double *resultat = &m.donnees[i][j];
    return resultat;
}

int nb_lignes_matrice(matrice m) {
    return m.l;
}

int nb_colonnes_matrice(matrice m) {
    return m.c;
}
